<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//This route is for logging out.
Route::get('/logout', [PostController::class, 'logout']);


//This route is for creation of a new post:
Route::get('/posts/create', [PostController::class, 'createPost']);

//This route is for saving the post on our database:
Route::post('/posts', [PostController::class, 'savePost']);

// This route is for the list of post on our database:
Route::get('/posts', [PostController::class, 'showPosts']);



/*----------------------------------------------------*/

// Activity S02:
// This route is for the welcome page
Route::get('/', [PostController::class, 'welcome']);

/*----------------------------------------------------*/

// S03 Discussion:
// Define a route that will return a view containing only the authenticated user's post
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

/*----------------------------------------------------*/

// Activity S03:
// Route for Edit specific Post page
Route::get('/posts/{id}/edit', [PostController::class, 'editPost']);

//This route is for saving the update on post on our database:
Route::put('/posts/{id}', [PostController::class, 'edit']);

/*----------------------------------------------------*/

// Activity S04:
// Route for Archive Post
Route::put('/posts/{id}/archive', [PostController::class, 'archive']);


/*----------------------------------------------------*/

// Dicussion S05:
// Define a web route that will call the function for liking and unliking a specific post:
Route::put('/posts/{id}/like', [PostController::class, 'like']);

/*----------------------------------------------------*/

// Activity S05:
// Route for Comment Post
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);
