App Folder - contains the models at its root. Models represents our database entities and have predefined methods for querying the respective tables that they represent.

	Http folder - Controllers subdirectory contains the project's controllers where we define our application logic.

database folder - migrations subdirectory contains the migrations that we will use to define the structures and the data types of our database tables.

public folder - where assets such as css, js images etc can be stored and accessed.

resources folder - views subdirectory is the namespace where all views will be looked for by our applications

routes folder - web.php file is where we define the routes of our application.

TO run your laravel application:
php artisan serve

To install Laravel's laravel/ui package via the terminal command:
composer require laravel/ui

Then build the authentication scaffolding via the terminal command:
php artisan ui bootstrap --auth

To create a Model:
php artisan make:model name_model -mc

to migrate:
php artisan migrate


to create a new migration:
php artisan make:migration create_post_likes_table