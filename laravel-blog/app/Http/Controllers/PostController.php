<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;
class PostController extends Controller
{

    //Controller is for the user logout
    public function logout(){
        // it will logout the currently logged in user.
        Auth::logout();
        return redirect('/login');
    }

    //action to return a view containing a form for a blog post creation
    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
        //to check whether there is an authenticated user or none
        if(Auth::user()){
            //instantiate a new Post Oject from the Post method and then save it in $post variable:
            $post = new Post;

            //define the properties of the $post object using the received form data
            $post->title=$request->input('title');
            $post->content=$request->input('content');

            //this will get the id of the authenticated user and set it as the foreign jet user_id of the new post.
            $post->user_id=(Auth::user()->id);

            //save the post object in our Post Table;
            $post->save();

            return redirect('/posts/create');
        }else{
            return redirect('/login');
        }
    }

    //controller will return all the the blog posts
    public function showPosts(){
        //the all() method will save all the records in our Post Table in our $post variable
        $posts = Post::all();

        return view('posts.showPosts')->with('posts', $posts);
    }


/*----------------------------------------------------*/

    // Activity S02:
    // Controller for welcome page
    public function welcome()
    {
        $randomPosts = Post::where('isActive', true)->inRandomOrder()->limit(3)->get();

        return view('welcome', ['randomPosts' => $randomPosts]);
    }

/*----------------------------------------------------*/

    // S03 Discussion
    // action for showing only the posts of the authored by authenticatged user
    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;
            return view('posts.showPosts')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }

    // action that will return a view showing a specific post using the URL parameter $id to query for the database entrry to be shown.
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);

    }

/*----------------------------------------------------*/
    
    // Activity S03:

    //action to return a view containing a form for editing blog post
    public function editPost($id){
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    // Edit and saving the update on our post
    public function edit(Request $request, $id){
        $post = Post::find($id);

        if (Auth::user() && Auth::user()->id == $post->user_id){

            $post->title = $request->input('title');
            $post->content = $request->input('content');

            $post->save();

            return view('posts.show')->with('post', $post);

        }else{
            return redirect('/login');
        }
    }

/*----------------------------------------------------*/
    // Activity S04:

    // Archive Post
    public function archive(Request $request, $id){
        $post = Post::find($id);

        if (Auth::user() && Auth::user()->id == $post->user_id){

            $post->isActive = false;

            $post->save();

            return redirect()->back();
            

        }else{
            return redirect('/login');
        }
    }

/*----------------------------------------------------*/
    // Discussion S05:
    public function like($id){
            $post = Post::find($id);

            if($post->user_id != Auth::user()->id){

                if($post->likes->contains("user_id", Auth::user()->id)){
                    //delete the like record made by the user before
                    PostLike::where('post_id', $post->id)->where('user_id', Auth::user()->id)->delete();
                }else{
                    //create a new like record to like this post
                    //instantiate a new PostLike object from the Postlike model
                    $postlike = new PostLike;
                    //define the properties of the $postlike
                    $postlike->post_id = $post->id;
                    $postlike->user_id = Auth::user()->id;

                    //save this postlike object in the database
                    $postlike->save();
                }
            }

        return redirect("/posts/$id");
    }


/*----------------------------------------------------*/
    // Activity S05:

    // Post Comment Controller
    public function comment(Request $request, $id){
        $post = Post::find($id);

        if(Auth::user()){

                $postcomment = new PostComment;
                    
                $postcomment->post_id = $post->id;
                $postcomment->user_id = Auth::user()->id;
                $postcomment->content = $request->input('content');

                $postcomment->save();

                
            }

        return redirect("/posts/$id");
    }


}




