@extends('layouts.app')

@section('tabName')
    Welcome
@endsection
@section('content')
    <div class="text-center">
        <div class="logo">
            <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="Laravel Logo" style="max-width: 800px; display: block; margin: 0 auto;">
        </div>
        <h2 class="mt-5">Featured Posts:</h2>
        <div class="card-columns">
            @foreach($randomPosts as $post)
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h5>
                        <p class="card-text">Author: {{$post->user->name}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
