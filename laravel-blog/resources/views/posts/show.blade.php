@extends('layouts.app')

@section('tabName')
	{{$post->title}}
@endsection
@section('content')
	<div class = "card col-6 mx-auto mb-5">
		<div class = 'card-body'>
			<h2 class = 'card-title'>{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<h4 class="mt-3">Content:</h4>
			<p class="card-text">{{$post->content}}</p>


			<p class="text-muted">Likes: {{$post->likes->count()}} | Comments: {{$post->comments->count()}}</p>
			


			@if(Auth::id() != $post->user_id)
				<form class = "d-inline" method = "POST" action = "/posts/{{$post->id}}/like">

					@csrf
					@method('PUT')
					@if($post->likes->contains('user_id', Auth::id()))
						<button class = "btn btn-danger">Unlike</button>
					@else
						<button class = "btn btn-success">Like</button>
					@endif


				</form>
			@endif

			@if(Auth::user())

				<button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">Comment</button>
				
				<form class = "d-inline" method = "POST" action = '/posts/{{$post->id}}/comment'>
					@csrf
					<div class="modal" tabindex="-1" id="commentModal">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title">Leave a Comment</h5>
					        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					      </div>
					      <div class="modal-body">
					        <h6>Comment:</h6>
					        <textarea class = "form-control" id = "content" name = "content" rows =3></textarea>
					      </div>
					      <div class="modal-footer">
					        <button type="submit" class="btn btn-primary">Comment</button>
					      </div>
					    </div>
					  </div>
					</div>


				</form>




			@endif


			<br/>



			<a href="/posts" class = "btn btn-info mt-2">View all posts</a>
			
		</div>
		
	</div>


	<h2 class = "col-6 mx-auto">Comments:</h2>
	@if (count($post->comments) > 0)
		@foreach ($post->comments as $comment)
			<div class = "card col-6 mx-auto mb-3">
				<div class = 'card-body'>
					<h3 class = 'card-title text-center'>{{$comment->content}}</h3>
					<p class="card-subtitle d-flex flex-column align-items-end mb-2 mt-2">Author: {{$comment->user->name}}</p>
					<p class="card-subtitle text-muted d-flex flex-column align-items-end">Created at: {{$comment->created_at}}</p>
					
				</div>
				
			</div>
		@endforeach
	@else
		<div class="col-6 mx-auto text-center">
		    <h4>No Comment yet</h4>
		</div>
	@endif

@endsection




