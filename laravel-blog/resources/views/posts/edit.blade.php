@extends("layouts.app")

@section('tabName')
	Edit Post
@endsection
@section('content')
	<form class = "col-6 p-5 mx-auto" method = "POST" action = "/posts/{{$post->id}}">
		@csrf
		@method('PUT')
		<div class = "form-group">
			<label for = 'title'>Title:</label>
			<input type="text" name="title" class = "form-control" id="title" value="{{$post->title}}" />
		</div>

		<div class = "form-group mt-3">
			<label for = "content">Content:</label>
			<textarea class = "form-control" id = "content" name = "content" rows =3>{{$post->content}}</textarea>
		</div>

		<div class = 'mt-2 '>
			<button class= "btn btn-primary" type="submit">Update Post</button>
		</div>
	</form>





@endsection